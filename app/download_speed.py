import wget
from timeit import default_timer as timer
import requests
import urllib.request
import shutil


def download_speed():
    source = 'https://file-examples.com/wp-content/uploads/2017/02/zip_5MB.zip'
    target = '../data/test/100MB.bin'
    iteration = 5

    # Test speed for wget module
    # Average Time for WGET module: 20.1627050508 seconds
    print('Start Test - WGET')
    start = timer()
    for i in range(iteration):
        print('Start Iteration ' + str(i + 1) + ' of ' + str(iteration))
        fh = wget.download(source, target)
    end = timer()
    time_wget = (end - start) / iteration
    print('Average Time for WGET module: ' + str(time_wget) + ' seconds')

    # Test speed for requests module
    # Average Time for Requests module: 19.7379191198 seconds
    print('Start Test - Requests')
    start = timer()
    for i in range(iteration):
        print('Start Iteration ' + str(i+1) + ' of ' + str(iteration))
        r = requests.get(source)
        with open(target, 'wb') as f:
            f.write(r.content)
    end = timer()
    time_requests = (end - start) / iteration
    print('Average Time for Requests module: ' + str(time_requests) + ' seconds')

    # Test speed for urllib.request module
    # Average Time for urllib.Request module: 19.645937967200002 seconds
    print('Start Test - urllib.Request')
    start = timer()
    for i in range(iteration):
        print('Start Iteration ' + str(i + 1) + ' of ' + str(iteration))
        with urllib.request.urlopen(source) as response, open(target, 'wb') as fh:
            shutil.copyfileobj(response, fh)
    end = timer()
    time_urllib = (end - start) / iteration
    print('Average Time for urllib.Request module: ' + str(time_urllib) + ' seconds')


if __name__ == '__main__':
    download_speed()
