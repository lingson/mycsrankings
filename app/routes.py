from flask import render_template, Markup, request, g
from app import app
from app.querydb import get_authors, get_pubs, get_urls
import json
import os
from app.myfunctions import get_special_url
from app.create_sqlitedb import get_country_list
from timeit import default_timer as timer


def get_html(selected):
    basedir = os.path.abspath(os.path.dirname(__file__))
    filename = os.path.join(basedir, 'static/ai_venues.json')

    with open(filename, 'r', encoding='utf-8') as fh:
        vlist = json.load(fh)

    html = '<input type="checkbox" name="cbox" value="all" onClick="toggle(this)"'
    if 'all' in selected:
        html += ' checked'
    html += '/>'
    html += '<span style="font-size: 14px;">Toggle All On/Off</span>\n<br>\n'
    for old_key in vlist.keys():
        key = old_key.replace('_', ' ').title()
        html += '<div class="row">\n'
        html += '<div class="col-11">\n'
        html += '<a href="#' + old_key + '" data-toggle="collapse">' + key + '</a>\n'
        html += '</div>\n'
        html += '<div class="col-1">\n'
        html += '<input type="checkbox" name="cbox" value="' + old_key + '" id="' + old_key + '"'
        html += ' onclick="changeCB(\'' + old_key + '\');"'
        if old_key in selected:
            html += ' checked'
        html += '>\n</div>\n</div>\n'
        html += '<div class="row collapse" id="' + old_key + '">\n'

        for item in vlist[old_key]:
            html += '<div class="col-10">' + item + '</div>\n'
            html += '<div class="col-2"><input type="checkbox" class="' + old_key + '" name="cbox" value="'
            html += item + '" onchange="submit();"'
            if item in selected:
                html += ' checked'
            html += '></div>\n'

        html += '</div>\n'
    html = Markup(html)
    return html


@app.route('/')
@app.route('/index')
def index():
    basedir = os.path.abspath(os.path.dirname(__file__))
    filename = os.path.join(basedir, 'static/pycsrankings.sqlite3')
    countries = get_country_list(filename)
    html = get_html([])
    return render_template('index.html', title='Home', html=html, countries=countries)


@app.route('/query', methods=['POST'])
def query():
    basedir = os.path.abspath(os.path.dirname(__file__))
    filename = os.path.join(basedir, 'static/pycsrankings.sqlite3')
    countries = get_country_list(filename)
    selected = request.form.getlist('cbox')
    country = request.form.get('country')
    yearstart = int(request.form.get('fromyear'))
    yearend = int(request.form.get('toyear'))
    start = timer()
    authors = get_authors(selected, country, yearstart, yearend)
    end = timer()
    print('get_authors: ' + str(end - start) + ' seconds')
    special = []
    for author in authors:
        name = author[1]
        urls = get_urls(name)
        special.append(get_special_url(name, urls))
    html = get_html(selected)
    return render_template('query.html', title='Author Rank', html=html,
                           authors=authors, pubs=[], countries=countries,
                           country=country, yearstart=yearstart, yearend=yearend, special=special)


@app.route('/author')
def author():
    name = request.args.get('name')
    pubs = get_pubs(name)
    urls = get_urls(name)
    special, other = get_special_url(name, urls)
    return render_template('author.html', name=name, pubs=pubs, special=special, urls=other)


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
