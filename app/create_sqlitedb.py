from tqdm import tqdm
import sqlite3
from sqlite3 import Error
import json
import os


def create_new_db(db_path: str, reset: bool = True, verbose: int = 1) -> None:
    """
    :param db_path: Where the SQLite file will be saved
    :param reset:
    :param verbose:
    :return:
    """
    if verbose:
        print("Creating the Database. ")
    if reset:
        os.remove(db_path)

    # Create new DB if no DB exist
    if not os.path.exists(db_path):
        try:
            conn = sqlite3.connect(db_path)
        except Error as e:
            print(e)
        finally:
            conn.close()
    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    # Table article
    sql = 'create table if not exists article ' \
          '(article_id integer primary key autoincrement, key text, title text, year integer, venue text);'
    c.execute(sql)
    sql = 'create index if not exists article_index on article (venue, article_id);'
    c.execute(sql)

    # Table author
    sql = 'create table if not exists author (author_id integer primary key autoincrement, name text);'
    c.execute(sql)
    sql = 'create index if not exists author_index on author (author_id, name);'
    c.execute(sql)

    # Table author_article
    sql = 'create table if not exists author_article ' \
          '(author_id integer, article_id integer, primary key (author_id, article_id));'
    c.execute(sql)
    sql = 'create index if not exists author_article_index on author_article (article_id);'
    c.execute(sql)

    # Table country
    sql = 'create table if not exists author_country ' \
          '(author_id integer, country TEXT, primary key (author_id, country));'
    c.execute(sql)
    sql = 'create index if not exists country_index on author_country (country, author_id);'
    c.execute(sql)

    # Table author_url
    sql = 'create table if not exists author_url (author_id integer, url TEXT, primary key (author_id, url));'
    c.execute(sql)
    sql = 'create index if not exists url_index on author_url (author_id);'
    c.execute(sql)

    conn.commit()
    conn.close()


def process_authors(db_name: str, file_path_pubs: str, verbose: int = 1) -> dict:
    if verbose:
        print("Loading the authors into the database. ")
    authors = set()
    with open(file_path_pubs, 'r', encoding='utf-8') as fh:
        if verbose:
            print("Loading set of authors. ")
        for line in tqdm(fh):
            info = json.loads(line)
            authors.update(info['author'])
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    author_ids = {}
    if verbose:
        print("Parsing authors into DB. ")
    for author in tqdm(authors):
        sql = 'insert into author(name) values (?)'
        p = (author,)
        author_ids[author] = c.execute(sql, p).lastrowid
    conn.commit()
    conn.close()
    return author_ids


def process_article_authors(db_name: str, file_path: str, author_ids: dict, verbose: int = 1) -> None:
    """
    :param db_name: Where the DB file is located
    :param file_path: Where the articles are
    :param author_ids:
    :param verbose:
    :return:
    """
    if verbose:
        print("Loading the articles and authors into the database. ")
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    def get_element(element, parse_int: bool = False):
        if isinstance(element, list):
            if len(element) == 1:
                if parse_int:
                    return int(element[0])
                return element[0]
            elif len(element) == 0:
                return None
        if parse_int:
            return int(element)
        return element

    with open(file_path, 'r', encoding='utf-8') as fh:
        for line in tqdm(fh):
            pub = json.loads(line)
            pub_key = get_element(pub['key'])
            pub_title = get_element(pub['title'])
            pub_year = get_element(pub['year'], parse_int=True)
            if pub['booktitle']:
                pub_venue = get_element(pub['booktitle'])
            elif pub['journal']:
                pub_venue = get_element(pub['journal'])
            else:
                pub_venue = None
            p = (pub_key, pub_title, pub_year, pub_venue)
            sql = 'INSERT INTO article(key, title, year, venue) VALUES (?,?,?,?);'
            c.execute(sql, p)
            pub_id = c.lastrowid

            # author & author_article
            for author in pub['author']:
                p = (author_ids[author], pub_id)
                sql = 'insert or ignore into author_article(author_id, article_id) values (?,?)'
                c.execute(sql, p)
    conn.commit()
    conn.close()


def process_countries(db_name: str, file_path: str, author_ids: dict, verbose: int = 1) -> None:
    if verbose:
        print("Loading the countries into the database. ")
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    sql = 'insert or ignore into author_country(author_id, country) values (?,?);'
    with open(file_path, 'r', encoding='utf-8') as fh:
        if verbose:
            print("Loading the countries. ")
        for line in tqdm(fh):
            info = json.loads(line)
            if info["author"].strip() in author_ids:
                p = (author_ids[info["author"].strip()], info["country"])
                c.execute(sql, p)
    conn.commit()
    conn.close()


def process_url(db_name: str, file_path: str, author_ids: dict, verbose: int = 1) -> None:
    if verbose:
        print("Loading the URLs into the database. ")
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    sql = 'insert or ignore into author_url(author_id, url) values (?,?);'
    with open(file_path, 'r', encoding='utf-8') as fh:
        for line in tqdm(fh):
            info = json.loads(line)
            authors = info.get('author')
            title = info.get('title')
            urls = info.get('url')
            if title == 'Home Page':
                if authors is not None and urls is not None:
                    if isinstance(authors, str):
                        authors = [authors]
                    if isinstance(urls, str):
                        urls = [urls]
                    for author in authors:
                        if author in author_ids:
                            for url in urls:
                                p = (author_ids[author], url)
                                c.execute(sql, p)
    conn.commit()
    conn.close()


def get_country_list(db_name: str):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()

    c.execute('select distinct country from author_country')
    countries = c.fetchall()
    countries = [x[0] for x in countries]
    countries.sort()
    conn.close()
    return countries


if __name__ == '__main__':
    file_author_country = '../data/author_countries.json'
    file_country = 'static/countries.txt'
    file_persons = '../data/persons.json'
    db = 'static/pycsrankings.sqlite3'
