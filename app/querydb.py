from flask import g
import sqlite3
import os


curpath = os.path.dirname(os.path.realpath(__file__))
DATABASE = os.path.join(curpath, 'static', 'pycsrankings.sqlite3')


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


def get_authors(venues: list, country: str, start_year: int, end_year: int, main=False) -> list:
    """
    :param venues:List of venues
    :param country: List of countries
    :param start_year: From when
    :param end_year: to when
    :param main:
    :return:
    """
    if main:
        conn = sqlite3.connect(DATABASE)
    else:
        conn = get_db()
    c = conn.cursor()

    if country == "all":
        sql = 'SELECT count(ar.article_id) as pubs, a.name FROM ' \
              '(select article_id from article WHERE venue in ({seq}) AND year >= ? AND year <= ?) as ar ' \
              'natural join author_article aa natural join author a ' \
              'GROUP BY a.author_id ORDER BY pubs DESC LIMIT 50;' \
              .format(seq=','.join(['?'] * len(venues)))
        p = venues
        p.append(start_year)
        p.append(end_year)
    else:
        sql = 'SELECT count(aa.article_id) as pubs, a.name FROM ' \
              '(select article_id from article WHERE venue in ({seq}) AND year >= ? AND year <= ?) as ar ' \
              'natural join author_article aa natural join' \
              '(select author_id, name from author a natural join author_country where country = ?) as a ' \
              'GROUP BY a.author_id ORDER BY pubs DESC LIMIT 50;' \
              .format(seq=','.join(['?'] * len(venues)))
        p = venues
        p.append(start_year)
        p.append(end_year)
        p.append(country)
    c.execute(sql, p)
    authors = c.fetchall()
    conn.close()
    return authors


def get_pubs(author_name: str):
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()

    # Change the SQL query because the old (join) one takes to long
    p = (author_name,)
    sql = 'select author_id from author where name = ?;'
    c.execute(sql, p)
    aid = c.fetchall()[0][0]

    p = (aid,)
    sql = 'select title, venue, year from article ar where ar.article_id in ' \
          '(select article_id from author_article where author_id = ?);'
    c.execute(sql, p)
    pubs = c.fetchall()
    conn.close()
    return pubs


def get_urls(author_name: str):
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()
    p = (author_name,)
    sql = 'select url from author_url natural join author a where a.name=?;'
    c.execute(sql, p)
    res = c.fetchall()
    result_urls = [x[0] for x in res]
    conn.close()
    return result_urls


if __name__ == '__main__':
    urls = get_urls('Zhi-Hua Zhou')
    print(urls)
