$(document).ready( function () {
    $('#publication').DataTable({
        "searching": false,
        "order": [[ 2, "desc" ]],
        "lengthChange": false,
        "pageLength": 50
    });

    $('#query').DataTable({
        "searching": false,
        "order": [[ 0, "asc"]],
        "lengthChange": false,
        "pageLength": 50,
        "paging": false,
        "info": false
    })
});

function toggle(source) {
    let checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (let i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
    let query = document.getElementById('queryform');
    query.submit();
}

function changeCB(venues) {
    let current = document.getElementById(venues);
    let checkboxes = document.getElementsByClassName(venues);
    let cb = true;
    if (current.checked == false) {
        cb = false;
    }
    for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = cb;
    }
    let query = document.getElementById('queryform');
    query.submit();
}

function closeWindow() {
    window.open('', '_parent', '');
    window.close();
}