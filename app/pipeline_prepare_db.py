from app.dblp_parser import parse_dblp, parse_dblp_person, get_dblp_country
from app.create_sqlitedb import process_article_authors, create_new_db, process_authors, process_url, process_countries
import urllib.request
import shutil
import gzip
import json
from tqdm import tqdm


URL = 'http://dblp.org/xml/'
URL_TEST = 'https://file-examples.com/wp-content/uploads/2017/02/zip_5MB.zip'
DATA_PATH = '../data/'
STATIC_PATH = 'static/'
TEST_PATH = '../data/test/'
FILE_GZ_DBLP = 'dblp.xml.gz'
FILE_GZ_TEST = 'test.xml.gz'
FILE_DTD = 'dblp.dtd'
FILE_TEST = '5MB.zip'
FILE_XML = 'dblp.xml'
FILE_XML_TEST = 'dblp2.xml'
FILE_ARTICLE = 'article.json'
FILE_AI_ARTICLE = 'ai_article.json'
FILE_AI_VENUE = 'ai_venues.json'
FILE_PERSON = 'persons.json'
FILE_AUTHOR_COUNTRY = 'author_countries.json'
FILE_COUNTRY = 'countries.txt'
DB = 'static/pycsrankings.sqlite3'


def download_dblp() -> None:
    source_gz = URL + FILE_GZ_DBLP
    source_dtd = URL + FILE_DTD
    target_gz = DATA_PATH + FILE_GZ_DBLP
    target_dtd = DATA_PATH + FILE_DTD

    print('Downloading file ' + source_gz)
    with urllib.request.urlopen(source_gz) as response, open(target_gz, 'wb') as fh:
        shutil.copyfileobj(response, fh)
    print('Downloading file ' + source_dtd)
    with urllib.request.urlopen(source_dtd) as response, open(target_dtd, 'wb') as fh:
        shutil.copyfileobj(response, fh)
    print('Download finish!')
    print()


def unzip_dblp() -> None:
    source = DATA_PATH + FILE_GZ_DBLP
    target = DATA_PATH + FILE_XML

    with gzip.open(source, 'rb') as f_in:
        with open(target, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    print()


def extract_publications():
    source = DATA_PATH + FILE_XML
    target = DATA_PATH + FILE_ARTICLE

    print('Parsing ' + source)
    parse_dblp(source, target)
    print('Parse finish! File ' + FILE_ARTICLE + ' created!')
    print()


def extract_ai_publications() -> list:
    source = DATA_PATH + FILE_ARTICLE
    source_venues = STATIC_PATH + FILE_AI_VENUE
    target_pubs = DATA_PATH + FILE_AI_ARTICLE

    authors = set()
    with open(source_venues, "r", encoding="utf-8") as f:
        tmp = json.loads(f.readlines()[0])
    venues = [a for b in tmp.values() for a in b]

    print('Parsing ' + source)
    with open(target_pubs, "w", encoding="utf-8") as out_f:
        with open(source, "r", encoding="utf-8") as in_f:
            for line in tqdm(in_f):
                line = json.loads(line)
                if line['booktitle']:
                    curr_venue = line['booktitle'][0]
                elif line['journal']:
                    curr_venue = line['journal'][0]
                for venue in venues:
                    if venue in curr_venue:
                        line['venue'] = venue
                        json.dump(line, out_f)
                        out_f.write("\n")
                        authors.update(line['author'])

    print('Parse finish! File ' + FILE_AI_ARTICLE + ' created!')
    print()
    return list(authors)


def extract_persons(author_list: list) -> None:
    source = DATA_PATH + FILE_XML
    target = DATA_PATH + FILE_PERSON

    print('Parsing ' + source)
    parse_dblp_person(source, target, author_list)
    print('Parse finish! File ' + FILE_PERSON + ' created!')
    print()


def parse_countries():
    source_person = DATA_PATH + FILE_PERSON
    source_country = STATIC_PATH + FILE_COUNTRY
    target = DATA_PATH + FILE_AUTHOR_COUNTRY

    print('Parsing ' + source_person)
    get_dblp_country(source_person, source_country, target)
    print('Parse finish! File ' + FILE_AUTHOR_COUNTRY + ' created!')
    print()


def create_db() -> None:
    source_article = DATA_PATH + FILE_AI_ARTICLE
    source_person = DATA_PATH + FILE_PERSON
    source_country = DATA_PATH + FILE_AUTHOR_COUNTRY
    target = DB

    create_new_db(target)
    author_ids = process_authors(db_name=target, file_path_pubs=source_article)
    process_article_authors(db_name=target, file_path=source_article, author_ids=author_ids)
    process_url(db_name=target, file_path=source_person, author_ids=author_ids)
    process_countries(db_name=target, file_path=source_country, author_ids=author_ids)
    print('DB Created!')
    print()


def pipeline_prepare_db() -> None:
    """
    '*** Starting pipeline process to prepare PyCSRankings Database ***'
    :return: None
    """
    print('**** Starting pipeline process to prepare PyCSRankings Database ****')
    print()

    print('Process 01 - Download DBLP data')
    download_dblp()

    print('Process 02 - Unzipping DBLP data')
    unzip_dblp()

    print('Process 03 - Create article.json')
    extract_publications()

    print('Process 04 - Create ai_article.json')
    author_list = extract_ai_publications()

    print('Process 05 - Creaate persons.json')
    extract_persons(author_list)

    print('Process 06 - Create author_countries.json')
    parse_countries()

    print('Process 07 - Create pycsrankings.sqlite3')
    create_db()

    print('*** Pipeline process to prepare PyCSRankings Database Finished! ***')


if __name__ == '__main__':
    # Set test_pipeline to True for development testing using mock (smaller) files/DBs
    # test = False
    pipeline_prepare_db()
